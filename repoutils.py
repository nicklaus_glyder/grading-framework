import sys, os, copy, tarfile, subprocess
from os import listdir
from os.path import isdir, isfile, join
from contextcd import cd

def UpdateRepos(sections):
    for section in sections:
        section_name = os.path.basename(section)
        print(f"Upating section {section_name}\n")
        with cd(section):
            proc = subprocess.Popen(['./update'])
            stdout, stderr = proc.communicate()
    return

def UntarAll(sections, assignment):
    is_archive = lambda x: x.endswith(".tar.gz") or x.endswith(".tar")

    # For each section
    for section in sections:
        # Lots of path setup
        section_name = os.path.basename(section)
        # Grab the assignment name
        assignment_path = join(section, "assignments", assignment)
        # Grab the students from the section bucket
        students = [s for s in listdir(assignment_path) if isdir(join(assignment_path, s))]
        students.sort()

        print(f"Untarring {assignment} for section {section_name}\n")

        # Just untar every submission
        for student in students:
            student_path = join(assignment_path, student)
            try:
                archives = [arch for arch in listdir(student_path) if is_archive(arch)]
                archive_path = join(student_path, archives[0])
                tar = tarfile.open(archive_path)
                tar.extractall(student_path)
                tar.close()
            except Exception as e:
                print(f"No usable archive found for student {student}\n")
                continue
