import os
from contextlib import contextmanager

# https://docs.python.org/2/library/contextlib.html#contextlib.contextmanager
# This pattern allows us to use a with: control block to locally change the
# execution directory of our code. This is useful for moving into a student's
# repo and compiling/running/etc.
@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)
    return
