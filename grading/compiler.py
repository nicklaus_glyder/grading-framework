import subprocess
import itertools
import locale
from collections import OrderedDict

class Compiler:
    """Class that can be used to compile programs from source and capture output
    Supports:
        C   (gcc, clang, make)
        C++ (g++, clang, make)
    """
    __source = []
    __stderr = ""
    __stdout = ""
    __encoding = locale.getdefaultlocale()[1]

    def __init__(self, source=[]):
        """Constructor
        'source' is a list of source files to compile
        """
        self.__source = source

    def compile(self, compiler="gcc", flags=OrderedDict()):
        """Compiles source files provided in constructor with the given compiler.
        Additional flags may be passed with the 'flags' hash as k,v pairs
        'compiler' is the compilation program to use, default is gcc
        Returns the exit code of the compiler
        """
        if compiler not in ['gcc', 'g++', 'clang']:
            raise 'Compiler ' + compiler + ' not supported'

        flag_list = list(itertools.chain(*(flags.items())))     # Flatten dict to list
        flag_list = list(filter(lambda x: x != '', flag_list))  # Remove blanks

        cmd = [compiler] + self.__source + flag_list
        print(f'Executing command: {cmd}\n')
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                    encoding=self.__encoding)

        self.__stdout, self.__stderr = proc.communicate()

        return proc.returncode

    def make(self, target=None):
        """Execute the Make command
        'target' is the make target to use, default is 'all' or first defined
        """
        # Generate command
        if target and target != "*":
            cmd = ['make', target]
        else:
            cmd = ['make']

        try:
            print(f'Executing command: {cmd}\n')
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        encoding=self.__encoding)

            self.__stdout, self.__stderr = proc.communicate()
        except Exception as e:
            print("Could not execute make target \'{target}\'")

        return proc.returncode

    def stdout(self):
        """Return contents of stdout buffer produced from the last compilation.
        If no compilation has been run, returns None.
        """
        return self.__stdout

    def stderr(self):
        """Return contents of stderr buffer produced from the last compilation.
        If no compilation has been run, returns None.
        """
        return self.__stderr
