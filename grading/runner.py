import subprocess
import threading
import os
import itertools
import locale
from collections import OrderedDict

class Runner:
    __executable = None
    __stderr = None
    __stdout = None
    __error = ''
    __encoding = locale.getdefaultlocale()[1]

    def __init__(self, executable=None):
        if os.path.isfile(executable) or os.path.isfile(executable.lower()):
            self.__executable = executable
        else:
            raise(f"Executable passed to constructor {executable} does not exist!")

    def run(self, args=OrderedDict(), input_lines=[], output=subprocess.PIPE, timeout=None, valgrind=False):
        """Run the specified executable
        'args' is a list of command line arguments to provide the executable
        'input' is a list of input strings to be passed to the proc's stdin
        'output' is where we want to direct the program's output, PIPE by default, pass in open File object
        'timeout' kill process after 'timeout' seconds if provided
        """
        args_list = list(itertools.chain(*(args.items())))     # Flatten dict to list
        args_list = list(filter(lambda x: x != '', args_list))       # Remove blanks

        # Generate command
        if valgrind:
            cmd = ['valgrind'] + ['--error-exitcode=7'] + ['--leak-check=full']
            cmd = cmd + [self.__executable] + args_list
        else:
            cmd = [self.__executable] + args_list

        print(f'Executing command: {cmd}\nInput:\n {input_lines}\n')
        proc = subprocess.Popen(cmd, stdout=output, stderr=output, stdin=subprocess.PIPE,
                                    encoding=self.__encoding)

        if timeout:
            def timeout_kill(p):
                print("Timeout, killing child process...\n")
                p.kill()
                self.__error = 'Timeout'
                return

            # Run student code with timeout
            timer = threading.Timer(timeout, timeout_kill, [proc])
            try:
                timer.start()
                out = proc.communicate("\n".join(input_lines))
                if output == subprocess.PIPE:
                    self.__stdout, self.__stderr = out
            finally:
                timer.cancel()
        else:
            out = proc.communicate("\n".join(input_lines))
            if output == subprocess.PIPE:
                self.__stdout, self.__stderr = out

        if proc.returncode == 7:
            self.__error = "Memory Leaks"

        return proc.returncode

    def make(self, target='run', input_lines=[], output=subprocess.PIPE, timeout=None):
        """Execute a make run target instead of executable invocation
        'target' is a valid make target
        'input' is a list of input strings to be passed to the proc's stdin
        'output' is where we want to direct the program's output, PIPE by default, pass in open File object
        'timeout' kill process after 'timeout' seconds if provided
        """
        cmd = ['make', target]

        try:
            print(f'Executing command: {cmd}\nInput:\n {input_lines}\n')
            proc = subprocess.Popen(cmd, stdout=output, stderr=output, stdin=subprocess.PIPE,
                                        encoding=self.__encoding)
            if timeout:
                def timeout_kill(p):
                    print("Timeout, killing child process...\n")
                    p.kill()
                    self.__error = 'Timeout'
                    return

                # Run student code with timeout
                timer = threading.Timer(timeout, timeout_kill, [proc])
                try:
                    timer.start()
                    out = proc.communicate("\n".join(input_lines))
                    if output == subprocess.PIPE:
                        self.__stdout, self.__stderr = out
                finally:
                    timer.cancel()
            else:
                out = proc.communicate("\n".join(input_lines))
                if output == subprocess.PIPE:
                    self.__stdout, self.__stderr = out

        except Exception as e:
            print("Could not execute 'make {target}'!")

        return proc.returncode

    def stdout(self):
        """Return contents of stdout buffer produced from the last run.
        If no compilation has been run, returns None.
        """
        return  self.__stdout

    def stderr(self):
        """Return contents of stderr buffer produced from the last run.
        If no compilation has been run, returns None.
        """
        return self.__stderr

    def error(self):
        """Return the error string set during the execution of the child process
        Empty string if no error
        """
        return self.__error
