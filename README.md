# CPSC C/C++ Grading Scripts
---
Python based AutoGrader for the grading and review programming assignments and labs.

## Required Software
---
### Git and Mercurial
Make sure you have Git installed, and Mercurial if grading from Handin repos.

### Python 3.6+
Please ensure you have Python 3.6+ available on your machine.
If not natively installed, I highly recommend downloading pyenv.

https://github.com/pyenv/pyenv#installation

## Writing A Grading Script
---
### JSON Config
Each AutoGrader job is controlled using a JSON config file stored in the `configs` directory. A simple example follows:

```json
{
  "compiler": {
    "program": "gcc",
    "files": [
      "test.c"
    ],
    "flags": { "-o": "DRIVER", "-Wall": "" },
    "make": { "target": "" }
  },
  "runner": {
    "executable": "DRIVER",
    "valgrind": true,
    "cla": { "arg1": "", "arg2": "stuff" },
    "input": [
      "first line of input",
      "next line of input"
    ],
    "timeout": 3
  },
  "grader": {
    "script": "configs.1021-spring18.labs.lab1.lab1",
    "package": "configs",
    "rubric": {
      "runs": { "max": 70, "value": 0, "comment": "" },
      "compiles": { "max": 30, "value": 0, "comment": "" }
    }
  }
}
```

Simply alter an existing config with the necessary values.
An explanation of each field follows:

NOTE: Do this...

### Python Grading Routines
Along with the JSON, you should write a simple Python script of the form:

```python
# Constants
PI = 3.14159

# Executed in student directory prior to grading
def pre_grade():
    # copy files or generate input here
    return

# Grading routine
def grade(output, rubric):
    rubric['runs']['value'] = rubric['runs']['max']
    return
```

The AutoGrader will call your pre_grade routine prior to executing student code.
This is a good place to generate input files, copy resources, etc.

Any variables defined in this script outside of the grading routines should be
effectively constant, since the dynamic module loader will only be run once.

The grade routine will be called after student code has been executed successfully.
This requires the student executable to return a 0 status code, so that stalled or
faulting programs don't interfere with the execution of the next student's submission.

Here you should assign points based on the textual or file-based output of the
program. See the example script for more info.  

## Grading Framework Technical Details
A simple framework has been developed to help speed script development. This framework allows us to compile, run, and report on C or C++ programs that the students will write.

It is advised that you check out the source code for yourself (it is a Python module that can be found in the `grading` package), but a quick explanation of each class and its pertinent methods is presented below:

### Compiler
The `Compiler` class is used to compile C/C++ source files into an executable. `Compiler` is not a singleton; each instance corresponds to a different job configuration. The output from `stdout` and `stderr` is captured for inspection.

- __\__init\____(*self*, *source=[]*)

Constructor, *source* is a list of all the .c or .cpp files that need to be compiled.

```python
c = Compiler(['lab1.c', 'helper.c'])
```

- __gcc_compile__(*self*, *flags={}*) / __clang_compile__(...)

Compile source files using gcc/clang, taking the optional `flags` parameter. `flags` is a dictionary consisting of key/value flags to pass to gcc/clang. The structure of the dictionary is as follows:

```python
flags = {
  '-o', 'lab1',
   '-Wall', '' # A key with no value takes the empty string
}
```

This function returns the exit code of gcc; a `0` denotes successful compilation.

```python
flags = {
  '-o', 'lab1',
  '-Wall', ''
}
exit_code = c.gcc_compile(flags)
# Executes the command:
# gcc -o lab1 -Wall lab1.c helper.c
```

- __stdout__() / __stderr__()

Return as a `string` the contents of the output buffers associated with the most recent compilation.

### Runner
The `Runner` class will execute a command and capture the output. Like `Compiler`, `Runner` is not a singleton and is associated with a single job configuration.

- __\__init\____(*self*, *executable=None*)

*executable* is just a string with the executable/command name we want to run. Will throw an error if none is supplied.

```python
r = Runner('./lab1')
```

- __run__(*self*, *args={}*, *input=[]*, *output=PIPE*, *timeout=None*)

Runs the executable/command job, with additional optional parameters.
Returns the exit code of the command/executable

*args* is a key/value dictionary of additional command line parameters. It takes the same form as the *flags* dictionary described above; any singular keys should be paired with the empty string.

*input* is a list of strings that will be fed to `stdin` during the execution of the program. Each item in the list will be fed to the program followed by a `\n`.

*output* is a File object that we will pipe output to. The default value is PIPE, which saves output in the Runner's output buffer.

*timeout* is a number in seconds, passing a value for *timeout* will automatically terminate the running command after the given number of seconds. This can be used to protect against infinite loops etc. A command killed in this way will return a negative exit code, and the *\__error* field on the `Runner` instance will be populated.

```python
r = Runner('./lab1')
args = {
  '-t', '50'
}
input = ['hello', 'world']

r.run(args, input, 5)

# Executes command
# ./lab1 -t 50
# with input: hello\n world\n
# times out after 5 seconds
```

- __stdout__() / __stderr__()

Return as a `string` the contents of the output buffers associated with the most recent execution.

- __error__()

Returns error string if any was populated during execution
