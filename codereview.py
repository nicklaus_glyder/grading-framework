import sys, os, copy, json, tarfile, csv, subprocess
from tempfile import NamedTemporaryFile
from os import listdir
from os.path import isdir, isfile, join
from contextcd import cd

def CodeReview(sections, assignment, code_file):
    print(f"Running CodeReview for assignment {assignment}\n")

    # For each section, we will visit all students and save logs
    for section in sections:
        # Lots of path setup
        section_name = os.path.basename(section)
        section_reports = join("./reports", section_name)
        assignment_report = join(section_reports, assignment)
        grade_csv_path = join(assignment_report, f"{section_name}-{assignment}.grades")
        review_csv_path = join(assignment_report, f"{section_name}-{assignment}.review")
        assignment_path = join(section, "assignments", assignment)

        # We only want to review after completing an AutoGrade routine
        if not isfile(grade_csv_path):
            print(f"Please run the AutoGrader for section {section_name}, no grade CSV found")
            continue

        # Grab the students from the section bucket
        students = [s for s in listdir(assignment_path) if isdir(join(assignment_path, s))]
        students.sort()

        print(f"Review for section {section_name}\n")

        # temp file for updating grades
        with open(grade_csv_path, "r") as grades, open(review_csv_path, 'w') as review:
            # Parse the existing grade file for quick lookups
            gradereader = csv.DictReader(grades, dialect='excel', restval='-')
            FIELD_NAMES = gradereader.fieldnames + ['format_penalty', 'adjusted_grade']
            gradedict = {}
            for row in gradereader:
                gradedict[row['Student']] = row

            # New records will be saved in a tempfile so we don't
            # have weird inconsistent states on early termination
            gradewriter = csv.DictWriter(review, fieldnames=FIELD_NAMES, dialect='excel', restval='-')
            gradewriter.writeheader()

            # Review each student in the section, note the use of the contextmanager
            # from above to move into student submission directory
            for student in students:
                student_path = join(assignment_path, student)
                code_path = join(student_path, code_file)
                student_grade = gradedict[student]

                # Student needs to have some initial grade
                try:
                    student_grade[assignment] = int(student_grade[assignment])
                except ValueError as e:
                    print(f'ERROR parsing initial grade value for {student}, skipping')
                    continue

                # Student needs to have a file matching the one chosen from the config
                if not isfile(code_path):
                    student_grade['format_penalty'] = '?'
                    student_grade['adjusted_grade'] = 0
                    gradewriter.writerow(student_grade)
                    print(f"No reviewable file found for student {student}\n")
                    continue

                # CODE IN THIS WITH: BLOCK IS IN STUDENT REPO
                with cd(student_path):
                    input(f'Reviewing {code_file} for student {student} : Press any key...\n')

                    # Run less to review a file, hit Q to exit
                    proc = subprocess.Popen(['less', code_file])
                    stdout, stderr = proc.communicate()

                    # Now we need a penalty score...
                    penalty = -1
                    while penalty < 0 or penalty > 100:
                        try:
                            penalty = int(input("Enter a penalty between 0 - 100: "))
                        except ValueError:
                            print(f'Invalid input {penalty}\n')
                            penalty = -1

                    student_grade['format_penalty'] = penalty
                    student_grade['adjusted_grade'] = student_grade[assignment] - penalty
                    gradewriter.writerow(student_grade)
                    print(f"\nAdjusted grade = {student_grade['adjusted_grade']}\n")
    return
