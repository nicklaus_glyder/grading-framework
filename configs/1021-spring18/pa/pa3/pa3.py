from os.path import join, abspath, dirname, isfile
import shutil

TEST_FILE = join(dirname(__file__), 'input.ppm')

# Copy grader input to student directory
def pre_grade():
    shutil.copyfile(TEST_FILE, 'input.ppm')
    return

# Grading routine
def grade(output, rubric):
    return
