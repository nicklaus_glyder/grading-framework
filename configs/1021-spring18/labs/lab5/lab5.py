import re

def pre_grade():
    return

PATTERN = r'^\s+[a-zA-Z0-9\s]*([\u2660-\u2663]{1})$'

# Grading routine
def grade(outputs, rubric):
    hand = []

    for line in output[0]:
        print(f"regex test: {line}")
        match = re.match(PATTERN, line, re.UNICODE)
        if match:
            try:
                hand.append(ord(match.group(1)))
                print("PASS")
            except Exception as e:
                pass
        else:
            break

    print(f"Hand Suits: {hand})")

    if len(hand) == 5:
        rubric['hand_size']['value'] = rubric['hand_size']['max']
    else:
        rubric['hand_size']['comment'] = "Hand less than 5 cards"
        return

    if all(a <= b for a,b in zip(hand[:-1], hand[1:])):
        rubric['hand_sorted']['value'] = rubric['hand_sorted']['max']
    else:
        rubric['hand_sorted']['comment'] = "Hand not sorted"

    return
