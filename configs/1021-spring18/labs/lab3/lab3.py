from os.path import join, abspath, dirname, isfile
import shutil

# This path is relative to this file (lab2.py)
TEST_FILE = join(dirname(__file__), 'mystery.dat')
OUT_FILE = 'bubble_5000.dat';

def pre_grade():
    shutil.copyfile(TEST_FILE, 'mystery.dat')
    return

# Grading routine
def grade(outputs, rubric):
    try:
        with open(OUT_FILE, 'r') as numbers:
            arr = [int(n) for n in numbers.readlines()]
            if len(arr) == 5000:
                rubric['output_length']['value'] = rubric['output_length']['max']
            else:
                rubric['output_length']['comment'] = "Didn't find 5000 ints"

            if len(arr) < 10:
                rubric['output_sorted']['comment'] = "Did not perform sort test"
                return

            if all(a <= b for a,b in zip(arr[:-1], arr[1:])):
                rubric['output_sorted']['value'] = rubric['output_sorted']['max']
            else:
                rubric['output_sorted']['comment'] = "Output not sorted"
        pass
    except Exception as e:
        rubric['output_sorted']['comment'] = str(e)
    return
