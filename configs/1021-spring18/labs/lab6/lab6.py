from os.path import join, abspath, dirname, isfile
from dateutil.parser import parse
import shutil

# This path is relative to this file (lab2.py)
TEST_FILE = join(dirname(__file__), 'test_dates.txt')
OUT_FILE = 'out.txt';

# Copy grader input to student directory
def pre_grade():
    shutil.copyfile(TEST_FILE, 'test_dates.txt')
    return

# Grading routine
def grade(outputs, rubric):
    try:
        with open(OUT_FILE, 'r') as dates_out:
            dates = []
            try:
                # Parse output of student code
                for l in dates_out.readlines():
                    dates.append(parse(l))

                # Output length test
                if len(dates) == 5:
                    rubric['input-output']['value'] = rubric['input-output']['max']
                elif len(dates) > 0:
                    rubric['input-output']['value'] = rubric['input-output']['max']/2
                    rubric['input-output']['comment'] = "Didn't find correct number of dates"
                else:
                    rubric['input-output']['comment'] = "Didn't find any dates"
                    raise ValueError('No dates!')

                # Output sortedness test
                if all(a <= b for a,b in zip(dates[:-1], dates[1:])):
                    rubric['sorted']['value'] = rubric['sorted']['max']
                else:
                    rubric['sorted']['comment'] = "Dates not sorted"

                pass
            except Exception as e:
                rubric['input-output']['comment'] = str(e)
        pass
    except Exception as e:
        rubric['input-output']['comment'] = str(e)
    return
