from os.path import join, abspath, dirname, isfile
from dateutil.parser import parse
import shutil

# This path is relative to this file (lab2.py)
TEST_FILE = join(dirname(__file__), 'lab11test.cpp')

# Copy grader input to student directory
def pre_grade():
    shutil.copyfile(TEST_FILE, 'lab11test.cpp')
    return

# Grading routine
def grade(outputs, rubric):
    return
