from os.path import join, abspath, dirname, isfile
import shutil
from PIL import Image
# ANY CODE EXECUTED HERE IS IN THE STUDENT SUBMISSION DIRECTORY!
# This means you can copy files from somewhere, re-run the program, etc.

# pre_grade and grade are functions that the AutoGrader looks for as hooks
# the grade function takes 2 params, the output from the students program
# and a rubric object matching the JSON file

# Executed in student directory prior to grading
# For Lab 2, we will copy a test PPM image

# This path is relative to this file (lab2.py)
TEST_IMAGE = join(dirname(__file__), 'test.ppm')
STUDENT_OUTPUT = 'p6.ppm'

def pre_grade():
    shutil.copyfile(TEST_IMAGE, 'test.ppm')
    return

# Grading routine
def grade(outputs, rubric):
    try:
        img = Image.open(STUDENT_OUTPUT)

        # Is the image header correct?
        if img.size == (128,128):
            rubric['correct_image_header']['value'] = rubric['correct_image_header']['max']
        else:
            rubric['correct_image_header']['comment'] = 'Incorrect Header'

        # Is it the right color?
        if img.getpixel((0,0)) == (0,0,255):
            rubric['correct_image_pixels']['value'] = rubric['correct_image_pixels']['max']
        else:
            rubric['correct_image_pixels']['comment'] = 'Could not parse PPM pixels'

    except Exception as e:
        rubric['correct_image_pixels']['comment'] = 'Error reading PPM'
        print(e)

    return
