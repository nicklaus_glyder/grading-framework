import sys, os, copy, json, tarfile, importlib, csv, subprocess
import shutil
from tempfile import NamedTemporaryFile
from os import listdir
from os.path import isdir, isfile, join
from contextlib import redirect_stdout

from grading.compiler import Compiler
from grading.runner import Runner
from contextcd import cd
from repoutils import UpdateRepos

# The underlying Compile/Run process. You can write scripts using these
# compile/run routines with even more freedom if you want, but a sensible
# default configurable by JSON is used by the AutoGrader
def grade_student(config, student, rubric, grade_routine):
    # Each student gets a grade log showing execution and results
    # This file is saved in their submission directory
    with open(student+".grade","w") as grade_file:
        with redirect_stdout(grade_file):
            compiler_cfg = config['compiler']
            runner_cfg = config['runner']

            # Compile the students submission using the JSON config as a guide
            # Check for warnings and errors
            compiler = Compiler(compiler_cfg['files'])

            if 'target' in compiler_cfg['make']:
                compiler_result = \
                compiler.make(target=compiler_cfg['make']['target'])
            else:
                compiler_result = \
                compiler.compile(compiler=compiler_cfg['program'], flags=compiler_cfg["flags"])

            if compiler_result == 0:
                print('=== Compilation Success ===\n')
                print(compiler.stdout() + compiler.stderr())

                # Compiler score set in rubric if the target exists
                if 'compiles' in rubric:
                    if compiler.stderr() == "":
                        rubric['compiles']['value'] = rubric['compiles']['max']
                    else:
                        rubric['compiles']['value'] = rubric['compiles']['max'] - 5
                        rubric['compiles']['comment'] = 'Compiler Warnings'

                # Run program with JSON config as guide, Valgrind supported
                # Program results are passed to the grading routine as lines of text
                outputs = []
                for run in runner_cfg['runs']:
                    if 'target' in runner_cfg['make']:
                        runner = Runner('Makefile')
                        return_code = runner.make(timeout=runner_cfg['timeout'],
                                            target=runner_cfg['make']['target'], input_lines=run['input'])
                    else:
                        runner = Runner('./' + runner_cfg['executable'])
                        return_code = runner.run(timeout=runner_cfg['timeout'], args=run['cla'],
                                            input_lines=run['input'], valgrind=runner_cfg['valgrind'])
                    if return_code == 0:
                        print('=== Execution Success ===\n')
                        print(runner.stdout())
                        if 'runs' in rubric:
                            rubric['runs']['value'] = rubric['runs']['max']/len(runner_cfg['runs'])
                        output_lines = [line.lower() for line in runner.stdout().split('\n')]
                        output_lines_no_blanks = list(filter(lambda x: x != '', output_lines))
                        outputs.append(output_lines_no_blanks)
                    else:
                        if runner.error() == 7:
                            print('!!! Valgrind Errors Detected !!!')
                        else:
                            print('!!! Execution Failed - {0} !!!'.format(runner.error()))
                        print(runner.stderr())

                # Actually run grade routines
                grade_routine(outputs, rubric)
                
            else:
                print('!!! Compilation Failed !!!')
                print(compiler.stdout() + compiler.stderr())
                if 'compiles' in rubric:
                    rubric['compiles']['comment'] = 'Failed to compile'

            print(json.dumps(rubric, indent=2))
    return

# Run the AutoGrader on the list of sections for a given assignment and JSON config
def AutoGrade(sections, assignment, config_path):
    with open(config_path) as config_file:
        # Dynamically load the pre_grade and grade routines
        config = json.load(config_file)
        script = config['grader']['script']
        package = config['grader']['package']
        grade_module = importlib.import_module(script, package=package)
        pre_grade_routine = grade_module.pre_grade
        grade_routine = grade_module.grade

        print(f"Running AutoGrader for assignment {assignment}\n")

        # First, update the buckets
        UpdateRepos(sections)

        # For each section, we will run all students and save logs
        for section in sections:
            # Lots of path setup
            section_name = os.path.basename(section)

            section_reports = join("./reports", section_name)
            if not isdir(section_reports):
                os.mkdir(section_reports)

            assignment_report = join(section_reports, assignment)
            if not isdir(assignment_report):
                os.mkdir(assignment_report)

            grade_csv_path = join(assignment_report, f"{section_name}-{assignment}.grades")
            assignment_path = join(section, "assignments", assignment)

            # Grab the students from the section bucket
            students = [s for s in listdir(assignment_path) if isdir(join(assignment_path, s))]
            students.sort()

            print(f"Grading for section {section_name}\n")
            with open(grade_csv_path, "w") as grades:
                FIELD_NAMES = ["Student", assignment]
                gradewriter = csv.DictWriter(grades, dialect='excel',
                                                fieldnames=FIELD_NAMES, restval="-")
                gradewriter.writeheader()

                # Grade each student in the section, note the use of the contextmanager
                # from above to move into student submission directory
                for student in students:
                    student_path = join(assignment_path, student)

                    try:
                        archives = [arch for arch in listdir(student_path) if arch.endswith(".tar.gz")]
                        archive_path = join(student_path, archives[0])
                        tar = tarfile.open(archive_path)
                        tar.extractall(student_path)
                        tar.close()
                    except Exception as e:
                        gradewriter.writerow({"Student": student, assignment: 0})
                        print(f"No usable archive found for student {student}\n")
                        continue

                    # CODE IN THIS WITH: BLOCK IS IN STUDENT REPO
                    with cd(student_path):
                        pre_grade_routine()
                        skip = False if 'skip' not in config['runner'] else config['runner']['skip']
                        if not skip:
                            rubric = copy.deepcopy(config['grader']['rubric'])
                            grade_student(config, student, rubric, grade_routine)
                            grade = sum([crit['value'] for crit in rubric.values()])

                            gradewriter.writerow({"Student": student, assignment: grade})
                            print(f"Grade for {student} = {grade}\n")
    return

# Same process as the general AutoGrader, but for a single student
# Note that we do not alter any CSV logs, but dump rubric to console
def AutoGradeSingle(section, assignment, student, config_path):
    with open(config_path) as config_file:
        config = json.load(config_file)
        script = config['grader']['script']
        package = config['grader']['package']
        grade_module = importlib.import_module(script, package=package)
        pre_grade_routine = grade_module.pre_grade
        grade_routine = grade_module.grade

        print(f"Running AutoGrader for {student}'s assignment {assignment}\n")

        # Get the path to the sections lab assignment
        assignment_path = join(section, "assignments", assignment)
        student_path = join(assignment_path, student)

        # untar the submission if possible, skip if not
        try:
            archive = [arch for arch in listdir(student_path) if arch.endswith(".tar.gz")]
            archive_path = join(student_path, archive[0])
            tar = tarfile.open(archive_path)
            tar.extractall(student_path)
            tar.close()
        except Exception as e:
            print(f"No usable archive found for student {student}\n")
            return

        # we are now in the student's directory
        with cd(student_path):
            pre_grade_routine()

            rubric = copy.deepcopy(config['grader']['rubric'])
            grade_student(config, student, rubric, grade_routine)
            grade = sum([crit['value'] for crit in rubric.values()])

            print("Grading rubric:\n")
            print(json.dumps(rubric, indent=2))
            print(f"Grade for {student} = {grade}\n")
    return
