import sys, os, json
import curses
from autograde import AutoGrade, AutoGradeSingle
from codereview import CodeReview
from repoutils import UpdateRepos, UntarAll
from os.path import isdir, join

# Genralized function for listing and selecting from a menu of options
def get_user_choice(screen, header_string, options):
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_WHITE)

    pages = [options[i:i+10] for i in range(0, len(options), 10)]
    page_index = 0
    current_page = pages[page_index]

    c = 0
    choice = 0
    while c != curses.KEY_ENTER and c != 10:
        screen.clear()
        w = screen.getmaxyx()[1]
        screen.addstr(w*'=' + header_string.center(w,' ') + w*'=')

        if len(pages) > 1:
            screen.addstr("\nUse LEFT and RIGHT for more options\n")

        # List options
        for num, option in enumerate(current_page) :
            if num == choice:
                attr = curses.color_pair(2)
            else:
                attr = curses.color_pair(1)
            label = (num+1)+(10*page_index)
            screen.addstr(f'{label}. ')
            screen.addstr(option + '\n', attr)
            pass
        # Draw screen
        screen.refresh()
        # Get input and update the menu selection
        c = screen.getch()
        if c == curses.KEY_UP and choice > 0:
            choice -= 1
        elif c == curses.KEY_DOWN and choice < len(options) - 1:
            choice += 1
        elif c == curses.KEY_RIGHT and page_index+1 < len(pages):
            page_index += 1
            current_page = pages[page_index]
        elif c == curses.KEY_LEFT and page_index-1 >= 0:
            page_index -= 1
            current_page = pages[page_index]
    return choice + (10*page_index)

def AutoGradeMenu(screen):
    # Select section to grade, or use them all
    sections = [join('./repos', d) for d in os.listdir('./repos') if isdir(join('./repos', d))]
    sections = ['All Sections'] + list(sorted(sections))
    choice = get_user_choice(screen, 'Select section(s) to AutoGrade', sections)
    sections = sections[1:] if choice == 0 else [sections[choice]]

    # Select an assignment to grade
    # NOTE: Make sure that they are named consistently across sections
    assignments = join(sections[0], 'assignments')
    assignment_list = list(sorted([a for a in os.listdir(assignments) if isdir(join(assignments, a))]))
    choice = get_user_choice(screen, 'Select the assignment', assignment_list)
    assignment = assignment_list[choice]

    # OS walk to select a config from the configs directory
    root = join('./configs')
    config = join('./configs')
    while not config.endswith('.json'):
        walk = [f for f in os.listdir(config) if isdir(join(config,f)) or f.endswith('.json')]
        walk = sorted(list(filter(lambda f: f != '__pycache__', walk)))
        if config != root:
            walk = walk + ['Back']
        choice = get_user_choice(screen, 'Select config file', walk)
        if walk[choice] == 'Back':
            config = os.path.split(config)[0]
        else:
            config = join(config, walk[choice])
    return (sections, assignment, config)

def AutoGradeSingleMenu(screen):
    # Select section to grade, or use them all
    sections = [join('./repos', d) for d in os.listdir('./repos') if isdir(join('./repos', d))]
    sections = list(sorted(sections))
    choice = get_user_choice(screen, 'Select section to AutoGrade', sections)
    section = sections[choice]

    # Select an assignment to grade
    assignments = join(section, 'assignments')
    assignment_list = list(sorted([a for a in os.listdir(assignments) if isdir(join(assignments, a))]))
    choice = get_user_choice(screen, 'Select the assignment', assignment_list)
    assignment = assignment_list[choice]

    # Select specific student
    students_path = join(assignments, assignment)
    student_list = list(sorted([s for s in os.listdir(students_path) if isdir(join(students_path, s))]))
    choice = get_user_choice(screen, 'Select student', student_list)
    student = student_list[choice]

    # OS walk to find a config to use from configs directory
    root = join('./configs')
    config = join('./configs')
    while not config.endswith('.json'):
        walk = [f for f in os.listdir(config) if isdir(join(config,f)) or f.endswith('.json')]
        walk = sorted(list(filter(lambda f: f != '__pycache__', walk)))
        if config != root:
            walk = walk + ['Back']
        choice = get_user_choice(screen, 'Select config file', walk)
        if walk[choice] == 'Back':
            config = os.path.split(config)[0]
        else:
            config = join(config, walk[choice])

    return (section, assignment, student, config)

def CodeReviewMenu(screen):
    # Select section to grade, or use them all
    sections = [join('./repos', d) for d in os.listdir('./repos') if isdir(join('./repos', d))]
    sections = ['All Sections'] + sorted(sections)
    choice = get_user_choice(screen, 'Select section(s) to AutoGrade', sections)
    sections = sections[1:] if choice == 0 else [sections[choice]]

    # Select an assignment to grade
    # NOTE: Make sure that they are named consistently across sections
    assignments = join(sections[0], 'assignments')
    assignment_list = list(sorted([a for a in os.listdir(assignments) if isdir(join(assignments, a))]))
    choice = get_user_choice(screen, 'Select the assignment', assignment_list)
    assignment = assignment_list[choice]

    # OS walk to select a config from the configs directory
    root = join('./configs')
    config = join('./configs')
    while not config.endswith('.json'):
        walk = [f for f in os.listdir(config) if isdir(join(config,f)) or f.endswith('.json')]
        walk = sorted(list(filter(lambda f: f != '__pycache__', walk)))
        if config != root:
            walk = walk + ['Back']
        choice = get_user_choice(screen, 'Select config file', walk)
        if walk[choice] == 'Back':
            config = os.path.split(config)[0]
        else:
            config = join(config, walk[choice])

    with open(config) as config_file:
        config_json = json.load(config_file)
        files = config_json['compiler']['files']
        choice = get_user_choice(screen, 'Select file to review', files)
        code_file = files[choice]

    return (sections, assignment, code_file)
    return

def UpdateReposMenu(screen):
    # Select section to grade, or use them all
    sections = [join('./repos', d) for d in os.listdir('./repos') if isdir(join('./repos', d))]
    sections = ['All Sections'] + sections
    choice = get_user_choice(screen, 'Select section(s) to Update', sections)
    sections = sections[1:] if choice == 0 else [sections[choice]]

    return (sections,)

def UntarMenu(screen):
    # Select section to grade, or use them all
    sections = [join('./repos', d) for d in os.listdir('./repos') if isdir(join('./repos', d))]
    sections = ['All Sections'] + sections
    choice = get_user_choice(screen, 'Select section(s) to Update', sections)
    sections = sections[1:] if choice == 0 else [sections[choice]]

    # Select an assignment to grade
    # NOTE: Make sure that they are named consistently across sections
    assignments = join(sections[0], 'assignments')
    assignment_list = list(sorted([a for a in os.listdir(assignments) if isdir(join(assignments, a))]))
    choice = get_user_choice(screen, 'Select the assignment', assignment_list)
    assignment = assignment_list[choice]

    return (sections, assignment)

# Uses the Python Curses bindings to make menu's a little easier
if __name__ == '__main__':
    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(True)
    curses.start_color()

    routines = [
        'AutoGrade',
        'AutoGrade (Single Student)',
        'CodeReview',
        'Update Repos',
        'UntarAll',
        'Quit']

    try:
        choice = get_user_choice(stdscr, 'Select a routine', routines)
        if routines[choice] == 'Quit':
            sys.exit(0)
        else:
            result = {
                'AutoGrade': AutoGradeMenu,
                'AutoGrade (Single Student)': AutoGradeSingleMenu,
                'CodeReview': CodeReviewMenu,
                'Update Repos': UpdateReposMenu,
                'UntarAll': UntarMenu
            } [routines[choice]](stdscr)
        pass
    except Exception as e:
        raise
    finally:
        curses.nocbreak()
        stdscr.keypad(False)
        curses.echo()
        curses.endwin()

    # This syntax is insane but useful
    {
        'AutoGrade': AutoGrade,
        'AutoGrade (Single Student)': AutoGradeSingle,
        'CodeReview': CodeReview,
        'Update Repos': UpdateRepos,
        'UntarAll': UntarAll
    } [routines[choice]](*result)
